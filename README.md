# Satis Private Repositories

[![build status](https://gitlab.com/agencia110-public/satis-metastore/badges/master/build.svg)](https://gitlab.com/agencia110-public/satis-metastore/commits/master)

[https://agencia110-public.gitlab.io/satis-metastore](https://agencia110-public.gitlab.io/satis-metastore)

Add `composer.json`:

```
"repositories": [{
    "type": "composer",
    "url": "https://agencia110-public.gitlab.io/satis-metastore"
}],
```
